**Generic Events System**

A simple and easy-to-use API for getting events working with both free functions and member functions!

これは簡単と使いやすいAPIで、メンバー関数とフリー関数、イーベントがどっちとでも使えます！

---

## Example (例)

```
#include "events.h"
#include <iostream>

namespace
{
    struct Foo : Events::EventHandler
    {
        Foo(Events::EventManager& manager) : Events::EventHandler(manager)
        {
            // register function into the event manager
            Register("Print", Events::details::to_function([](int count, float f, double d)
            {
                std::cout << "Foo " << count * f * d << '\n';
            }));
        }
    };

    struct Bar : Events::EventHandler
    {
        Bar(Events::EventManager& manager) : Events::EventHandler(manager)
        {
            // register function into the event manager
            Register("Print", Events::details::to_function([](int count, float f, double d)
            {
                std::cout << "Bar " << count << ' ' << f << ' ' << d << '\n';
            }));
        }
    };
}

int main()
{
    // create the event manager
    Events::EventManager manager;

    // create objects which extend event handlers
    Foo f(manager);
    Bar b(manager);

    // getting information at runtime
    int i;
    std::cin >> i;

    // queue events
    manager.Queue("Print", 10, std::move(i), 1.1f, 0.2);
    manager.Queue("Print", 30, 1, 30.4f, 0.3);
    manager.Queue("Print", 1,  2, 0.001f, 0.4);

    // fire all queued events
    manager.FireQueued();

    // specify argument types
    try
    {
        manager.Fire<int, float, double>("Print", 5, 2, 0.001);
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << '\n';
    }

    // external handler
    Events::EventHandler handler{manager};
    handler.RegisterOnce("Hello", Events::details::to_function([]{ std::cout << "Hello, World!\n"; }));
    manager.Fire("Hello");

    return 0;
}
```

---

## How To Use (使い方)

All of the documentation for this system lies in [events.h](events.h). Please give that a read-through before trying to do anything else.

全てのドキュメントは[events.h](events.h)にあります。使う前に、徹底的に読んで下さい。

## Public Functions (パブリック関数)

- to_function
- TotalQueued
- Fire
- Queue
- FireQueued
- EventHandler
- ~EventHandler
- Register
- RegisterOnce
- RemoveListener

## License (ライセンス)

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

このプロジェクトは、MIT Licenseの元に免許されています。もっと情報は[LICENSE.md](LICENSE.md)にあります。