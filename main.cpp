/******************************************************************************/
/*
* @file   main.cpp
* @author Aditya Harsh
* @brief  Generic event-system implementation.

          Copyright (c) 2018 Aditya Harsh
          Permission is hereby granted, free of charge, to any person obtaining 
          a copy of this software and associated documentation files 
          (the "Software"), to deal in the Software without restriction, 
          including without limitation the rights to use, copy, modify, merge, 
          publish, distribute, sublicense, and/or sell copies of the Software, 
          and to permit persons to whom the Software is furnished to do so, 
          subject to the following conditions:
          The above copyright notice and this permission notice shall be 
          included in all copies or substantial portions of the Software.
          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
          EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
          MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
          NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
          BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
          ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
          CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
          SOFTWARE.
*/
/******************************************************************************/

#include "events.h"
#include <iostream>

namespace
{
    struct Foo : Events::EventHandler
    {
        Foo(Events::EventManager& manager) : Events::EventHandler(manager)
        {
            // register function into the event manager
            Register("Print", Events::details::to_function([](int count, float f, double d)
            {
                std::cout << "Foo " << count * f * d << '\n';
            }));
        }
    };

    struct Bar : Events::EventHandler
    {
        Bar(Events::EventManager& manager) : Events::EventHandler(manager)
        {
            // register function into the event manager
            Register("Print", Events::details::to_function([](int count, float f, double d)
            {
                std::cout << "Bar " << count << ' ' << f << ' ' << d << '\n';
            }));
        }
    };
}

int main()
{
    // create the event manager
    Events::EventManager manager;

    // create objects which extend event handlers
    Foo f(manager);
    Bar b(manager);

    // getting information at runtime
    int i;
    std::cin >> i;

    // queue events
    manager.Queue("Print", 10, std::move(i), 1.1f, 0.2);
    manager.Queue("Print", 30, 1, 30.4f, 0.3);
    manager.Queue("Print", 1,  2, 0.001f, 0.4);

    // fire all queued events
    manager.FireQueued();

    // specify argument types
    try
    {
        manager.Fire<int, float, double>("Print", 5, 2, 0.001);
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << '\n';
    }

    // external handler
    Events::EventHandler handler{manager};
    handler.RegisterOnce("Hello", Events::details::to_function([]{ std::cout << "Hello, World!\n"; }));
    manager.Fire("Hello");

    return 0;
}