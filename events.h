/******************************************************************************/
/*
* @file   events.h
* @author Aditya Harsh
* @brief  Generic event-system implementation.

          Copyright (c) 2018 Aditya Harsh
          Permission is hereby granted, free of charge, to any person obtaining 
          a copy of this software and associated documentation files 
          (the "Software"), to deal in the Software without restriction, 
          including without limitation the rights to use, copy, modify, merge, 
          publish, distribute, sublicense, and/or sell copies of the Software, 
          and to permit persons to whom the Software is furnished to do so, 
          subject to the following conditions:
          The above copyright notice and this permission notice shall be 
          included in all copies or substantial portions of the Software.
          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
          EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
          MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
          NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
          BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
          ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
          CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
          SOFTWARE.
*/
/******************************************************************************/

#ifndef EVENTS
#define EVENTS

#include <string>                                   // std::string
#include <functional>                               // std::function
#include <unordered_map>                            // std::unordered_map
#include <unordered_set>                            // std::unordered_set
#include <vector>                                   // std::vector
#ifdef __APPLE__
    #include <experimental/tuple>                   // std::tuple
#else
    #include <tuple>                                // std::tuple
#endif
#include <queue>                                    // std::priority_queue
#include <boost/any.hpp>                            // boost::any, boost::any_cast
#include <boost/callable_traits/args.hpp>           // boost::callable_traits::args_t
#include <boost/callable_traits/return_type.hpp>    // boost::callable_traits::return_type_t
#include <boost/callable_traits/apply_return.hpp>   // boost::callable_traits::apply_return_t

namespace Events
{
    namespace details
    {
        /**
         * @brief Converts a callable object into a std::function.
         * 
         * @tparam F Function type
         * @param f Function
         * @return auto std::function representing the callable
         */
        template <typename F>
        constexpr auto to_function(F&& f) noexcept
        {
            using ReturnType = boost::callable_traits::return_type_t<F>;
            using ArgsType = boost::callable_traits::args_t<F>;
            using FunctionType = std::function<boost::callable_traits::apply_return_t<ArgsType, ReturnType>>;
            return FunctionType(std::forward<F>(f));
        }
    }

    // forward declare
    class EventHandler;

    /**
     * @brief Manages the events system.
     * 
     */
    class EventManager final
    {
        friend class EventHandler;

        // used for storing events inside the priority queue
        struct EventQueue
        {
            std::function<void()> callback_;
            size_t priority_;
            constexpr bool operator<(const EventQueue& other) const noexcept { return priority_ < other.priority_; }
        };

        std::unordered_map<std::string, std::vector<std::tuple<EventHandler*, boost::any, bool>>> fire_queue_;
        std::priority_queue<EventQueue> delay_queue_;

    public:

        /**
         * @brief Gets the total number of queued events.
         * 
         * @return size_t The number of queued events.
         */
        constexpr size_t TotalQueued() const noexcept
        {
            return delay_queue_.size();
        }

        /**
         * @brief Fires callbacks for all listeners in one go.
         * 
         * @tparam Args Variadic arguments.
         * @param event_name The name of the event to call.
         * @param args All of the arguments to pass to the event.
         */
        template <typename... Args>
        void Fire(const std::string& event_name, Args... args)
        {
            auto& queue = fire_queue_.at(event_name);
            
            std::vector<EventHandler*> erase_list;
            
            for (auto&& i : queue)
            {
                boost::any_cast<std::function<void(Args...)>>(std::get<1>(i))(std::forward<Args>(args)...);

                if (std::get<2>(i))
                    erase_list.emplace_back(std::get<0>(i));
            }

            // remove flagged events
            for (auto&& i : erase_list)
                RemoveListener(event_name, i);
        }

        /**
         * @brief Queues an event.
         * 
         * @tparam Args Variadic arguments.
         * @param event_name The name of the event to call.
         * @param priority The priority of the queued event.
         * @param args All of the arguments to pass to the event.
         */
        template <typename... Args>
        constexpr void Queue(const std::string& event_name, size_t priority, Args... args)
        {
            delay_queue_.emplace
            (
                EventQueue
                {
                    [this, event_name, tup = std::make_tuple(std::forward<Args>(args)...)] () mutable
                    {
                        auto f = [this, &event_name](auto&&... a)
                        {
                            Fire(event_name, std::forward<Args>(a)...);
                        };

#ifdef __APPLE__
                        std::experimental::apply(f, tup);
#else
                        std::apply(f, tup);
#endif
                    },
                    priority
                }
            );
        }

        /**
         * @brief Fires off queued events.
         * 
         * @param count Number of events to fire. A count of zero means fire all events.
         */
        void FireQueued(size_t count = 0)
        {
            if (count > delay_queue_.size())
                throw std::logic_error("Too many delayed events are attempting to be fired!");

            const auto callback = [this] // keep popping off the queue
            {
                auto& cb = delay_queue_.top();
                cb.callback_();
                delay_queue_.pop();
            };

            if (count == 0)
            {
                while (!delay_queue_.empty())
                    callback();
            }
            else
            {
                for (size_t i = 0; i < count; ++i)
                    callback();
            }
        }

    private:

        /**
         * @brief Adds a listener to the event map.
         * 
         * @tparam T Type of the function.
         * @param event_name Name of the event.
         * @param handler Pointer to the event handler.
         * @param function The function to add.
         * @param fire_once Whether or not the listener should be removed after firing.
         */
        template <typename T>
        constexpr void AddListener(const std::string& event_name, EventHandler* handler, T&& function, bool fire_once)
        {
            auto& vec = fire_queue_[event_name];
            vec.emplace_back(std::make_tuple(handler, std::move(function), fire_once));
        }

        /**
         * @brief Removes a listener from an event.
         * 
         * @param event_name The name of the event to remove from.
         * @param handler The handler to remove.
         */
        void RemoveListener(const std::string& event_name, const EventHandler* handler)
        {           
            auto it = fire_queue_.find(event_name);

            if (it == fire_queue_.cend())
                throw std::logic_error("Attempting to remove listener from an event that doesn't exist!");

            auto& vec = it->second;

            for (size_t i = 0; i < vec.size(); ++i)
            {
                if (std::get<0>(vec[i]) == handler)
                {
                    vec.erase(std::begin(vec) + i);

                    if (vec.empty())
                        fire_queue_.erase(it);

                    return;
                }
            }

            throw std::runtime_error("The listener seems to have disappeared!");         
        }
    };

    /**
     * @brief Event handler. Inherit from this class.
     * 
     */
    class EventHandler
    {
        // manager to use for events
        EventManager& manager_;

        // allows for quick removal of events from the manager
        std::unordered_set<std::string> registered_;

    public:

        /**
         * @brief Construct a new Event Handler object.
         * 
         * @param manager The event manager to use.
         */
        EventHandler(EventManager& manager) noexcept : manager_{manager} {}

        /**
         * @brief Destroy the Event Handler object.
         * 
         */
        ~EventHandler()
        {
            for (auto&& i : registered_)
                manager_.RemoveListener(i, this);
        }

        /**
         * @brief Registers a function callback.
         * 
         * @tparam Args The parameters of the function callback.
         * @param event_name The name of the event.
         * @param function The function to callback.
         */
        template <typename... Args>
        constexpr void Register(const std::string& event_name, std::function<void(Args...)> function)
        {
            if (registered_.find(event_name) != registered_.cend())
                throw std::logic_error("Attempting to register for the same event more than once!");

            manager_.AddListener(event_name, this, function, false);

            registered_.emplace(event_name);
        }

        /**
         * @brief Registers a function callback for a one-time event.
         * 
         * @tparam Args The parameters of the function callback.
         * @param event_name The name of the event.
         * @param function The function to callback.
         */
        template <typename... Args>
        constexpr void RegisterOnce(const std::string& event_name, std::function<void(Args...)> function)
        {
            if (registered_.find(event_name) != registered_.cend())
                throw std::logic_error("Attempting to register for the same event more than once!");

            manager_.AddListener(event_name, this, function, true);
        }
        
        /**
         * @brief Removes a listener from all subscribed events.
         * 
         */
        void RemoveListener()
        {
            for (auto&& i : registered_)
                manager_.RemoveListener(i, this);

            registered_.clear();
        }

        /**
         * @brief Removes a listener from a specific event.
         * 
         * @param event_name Event to remove from.
         */
        void RemoveListener(const std::string& event_name)
        {
            auto it = registered_.find(event_name);

            if (it == registered_.cend())
                throw std::logic_error("Not subscribed to event!");

            manager_.RemoveListener(event_name, this);

            registered_.erase(it);
        }
    };
}

#endif
